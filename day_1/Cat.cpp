#include "Cat.hpp"
#include <iostream>

Cat::Cat() : weight(5), name("kot") {};

Cat::Cat(uint8_t w) : weight(w), name("kot") {};

Cat::Cat(std::string name) : weight(5), name(name) {};

Cat::~Cat() {
   std::cout << Cat::name << std::endl;
}

void Cat::meow(uint8_t howLoud) {
   std::cout << "Meow";
   for (int i = 0; i < howLoud; ++i) {
      std::cout << '!';
   }
   std::cout << std::endl;
}

void Cat::setName(std::string name) {
   this->name = name;
}

std::string Cat::getName() {
   return name;
}
