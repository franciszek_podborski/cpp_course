#ifndef RECTANGLE_H_
#define RECTANGLE_H_

class Rectangle {
private:
   int height;
   int width;

public:
   int area();

   int getHeight() {
      return height;
   }
   int getWidth() {
      return width;
   }
};

#endif
