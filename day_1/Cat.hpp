#ifndef CAT_HPP_
#define CAT_HPP_

#include <iostream>

class Cat {
private:
   uint8_t weight;
   std::string name;
public:
   Cat();
   Cat(uint8_t w);
   Cat(std::string name);
   ~Cat();

   void setName(std::string name);
   std::string getName();

   void meow(uint8_t howLoud);
};

#endif
